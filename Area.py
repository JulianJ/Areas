"""
Tecnológico de Costa Rica
Area de una esfera
Julian J Aguilera Calderón
Sebastían Calderón Yock
Version 1.0
Entradas: Radio de esfera
Restricciones: No se puede radio negativo
Salida: Are de la esfera
"""
from math import pi

def Area_Esf(Radio):
    if Radio>0:
        return A_E_aux(Radio)
    else:
        return "Numero debe ser positivo"

def A_E_aux(Radio):
    A=4*pi*pow(Radio,2)
    return A


"""
Tecnológico de Costa Rica
Area de una esfera
Julian J Aguilera Calderón
Sebastían Calderón Yock
Version 1.0
Entradas: Lado de la base y altura de la piramide
Restricciones: No se puede entradas negativas
Salida: Are de la piramide
"""

def Area_Pir(LBase,Altura):
    if LBase>0 and Altura >0:
        return A_P_aux(LBase,Altura)
    else:
        return "Numeros deben ser positivos"

def A_P_aux(LBase,Altura):
    Ab=pow(LBase,2)
    Hp=pow((Ab+pow(Altura,2)),1/2)
    At=(LBase*Hp)/2
    return 3*At+Ab

